import { Component, OnInit } from '@angular/core';
// import { AuthService } from '../services/auth.service';
// import { LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

import { Producto } from './productos';
import { PRODUCTOS } from './listaProductos';
// import { from } from 'rxjs';
import { CarritoService } from '../services/carrito-service.service';
import {ResultadoFinal}  from './precioFinal';
import {ResultadoPrecioFinal} from './resultadoPrecioFinal';
// import {ResultadoFinal} from './precioFinal';

// import { Router } from '@angular/router';


@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  carrito = [];
  carritoProductos=[];
  // productos: Producto[] = [];
  searchTerm : string = "";
  searchededItems: Producto[];
  showing: string = "products";
  selectedItems = [];
  // filteredItems = [];
  status: string = 'products';
 
  // resultadoFinal=this.calcularPrecioFinal();
  // resultadoFinal = ResultadoFinal;
  // ResultadoPrecioFinal=this.calcularPrecioFinal();

  PrecioResultadoFinal;
  

  // resultadoFinal=ResultadoPrecioFinal;

  
  productos = PRODUCTOS;
  // precioFinal;

  constructor(
   
    // public loadingCtrl: LoadingController,
    // private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,

    private carritoService: CarritoService
  ) { }


  ngOnInit() {
    this.carrito = this.carritoService.getCarrito();
    console.log("Productos Carrito cargados en carrito.page: ", this.carrito);
    
  }

  ngDoCheck(){
  // this.carrito = this.carritoService.carritoProductos;}
  // this.carrito=this.carritoService.getCarrito()
  // this.carrito=this.carritoService.addCarrito(Producto);
  
  // this.totalPriceProductos = this._productosService.getTotalPrice();
    this.carritoProductos = this.carritoService.getCarrito();

  }
  

  addCarrito(producto){
    this.carritoService.addCarrito(producto);
  }

  removeCarrito(producto){
    this.carritoService.removeCarrito(producto);
  }
  // calcularPrecioFinal(producto){
  //   this.carritoService.calcularPrecioFinal(producto);
  // }




  // En el caço de hacerlo solo con un Type sript que sea directamente Array

  /*  producto: Productos[] = [
     {
       id: 'GR1',
       name: 'Green tea',
       price: 3.11
     },
     {
       id: 'SR1',
       name: 'Strawberries',
       price: 5.00,
 
     },
     {
       id: 'CF1',
       name: 'Coffee',
       price: 11.23,
 
     }
   ]; */
  
  
  
   //  Es funcional
  //  addCarrito(producto) {
  //   console.log('se va añadir al carrito',this.carritoProductos);
    
  //   if (producto.counter===0){
  //   this.carritoProductos.push(producto);
  //   producto.counter++;
  // } else{
  //   producto.counter++;
  // }

  //  toggleCarrito(producto) {
  //   if(producto.carrito && producto.carrito == true) {
  //     this.carritoService.removeCarrito(producto);
  //   }
  //   else{
  //     this.carritoService.addCarrito(producto);
  //   }
 
// }

calcularPrecioFinal(){

  this.PrecioResultadoFinal=this.carritoService.calcularPrecioFinal();

}
 

}
