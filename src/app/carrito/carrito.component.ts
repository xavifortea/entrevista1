import { Component, OnInit } from '@angular/core';
import { CarritoService } from '../services/carrito-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  carrito = [];
  carritoProductos=[];
  // productos: Producto[] = [];
  searchTerm : string = "";
  
  showing: string = "products";
  selectedItems = [];
  // filteredItems = [];
  status: string = 'products';

  constructor(
    private router: Router,
    private route: ActivatedRoute,

    private carritoService: CarritoService,
  ) { }

  ngOnInit() {
    // this.carrito=this.carritoService.carritoProductos;
  }

    ngDoCheck(){
      this.carrito=this.carritoService.getCarrito();
    }
  



}
