import { Injectable } from '@angular/core';
import { Producto } from '../productos/productos';
// import {precioFinal} from '../productos/precioFinal';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {

  selectedProducto;
  carritoProductos = [];
  resultadoFinal = this.calcularPrecioFinal();
  // resultadoFinal=this.calcularPrecioFinal();
  constructor() { }

  setSelectedProduct(producto) {
    this.selectedProducto = producto;
  }

  getSelectedProduct() {
    return this.selectedProducto;
  }

  addCarrito(producto) {
    // console.log('se va añadir al carrito',this.carritoProductos);
    if (producto.counter == 0) {
      this.carritoProductos.push(producto);
      producto.counter++;
      producto.precioTotal = producto.priece * producto.counter;
      // console.log("En el carrito hay: ", this.carritoProductos)
    } else {
      producto.counter++;
      producto.precioTotal = producto.priece * producto.counter;
      // console.log("En el carrito hay: ", this.carritoProductos)
    }
    // condicional con contador en el carrito
    producto.carrito = true;
    console.log("En el carrito hay: ", this.carritoProductos);
    if (producto.name == 'Strawberries' && producto.counter >= 3) {
      // producto.priece = 4.50 * producto.counter; el precio final
      producto.priece = 4.50;
      producto.precioTotal = producto.priece * producto.counter +1;
      //  el mas 1 es la diferncia que habria entre 2 de 5 euros y 2 de 4,50 osea las 2 de 5 son 10 y 2 de 4,50 son 9
    } else if (producto.name == 'Green tea') {
      producto.counter++;
      producto.precioTotal = producto.priece * (producto.counter/2);
    }
    else if (producto.name == 'Coffee' && producto.counter >= 3) {
      // producto.priece = 7, 42 * producto.counter;
      producto.priece = 7.42;
      producto.precioTotal = producto.priece * producto.counter;
    }
    console.log(producto.precioTotal);
    
  }

  // removeCarrito(producto) { este es el de Luis de wakapop
  //   producto.carrito = false;
  //   this.carritoProductos.map((currElement, index) => {
  //     if (currElement.id == producto.id) {
  //       this.carritoProductos.splice(index, 1);
  //     }
  //   });
  //   console.log("Los favoritos son: ", this.carritoProductos);
  // }

  removeCarrito(producto) {
    if (producto.counter == 1) {
      this.carritoProductos.splice(producto);
      producto.counter--;
    }
    else if (producto.counter == 0) {
    producto.counter = 0;
    producto.precioTotal = producto.priece * producto.counter;
    }
    else {
    producto.counter--;
      producto.precioTotal = producto.priece* producto.counter;
    }

    if (producto.name == 'Strawberries' && producto.counter < 3) {

      producto.priece = 5;
      producto.precioTotal = producto.priece * producto.counter;
    }
    /* else if (producto.name == 'Green tea') {
     producto.counter++;
   } */
    else if (producto.name == 'Coffee' && producto.counter < 3) {

      producto.priece = 11.23;
      

      producto.precioTotal = producto.priece * producto.counter;
      
    }
    else if (producto.name == 'Green tea' 
    // && 
    // producto.counter < 3
    ) {

      producto.counter--; 
      

      producto.precioTotal = producto.priece * (producto.counter/2);
      
    }
    // 7.42


    console.log(this.carritoProductos);
    console.log(producto.precioTotal);
  }

  getCarrito() {
    return this.carritoProductos;
  }
  sumaProductos(producto){
    var precioFinal= producto[1].precioTotal + producto[2].precioTotal+ producto[0].precioTotal;
    return precioFinal;
    
  }

  // cambioPrecio(producto) {
  //   if (producto.name == 'Strawberries' && producto.counter >= 3) {
  //     producto.priece = 4.50 * producto.counter;
  //   } else if (producto.name == 'Green tea') {
  //     producto.counter++;
  //   }
  //   else if (producto.name == 'Coffee' && producto.counter >= 3) {
  //     producto.priece = 7, 42 * producto.counter;
  //   }

  // }
   calcularPrecioFinal() {
    // var resultadoPrecioFinal;
    var resultadoFinal=0;
    for (var i=0; i<this.carritoProductos.length; i++){

      resultadoFinal +=   this.carritoProductos[i].precioTotal
      

    }
    console.log('el resultadofinal es:'+ resultadoFinal);
    return resultadoFinal;
  }
}

